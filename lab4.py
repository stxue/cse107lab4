import random
import math

def main():
  k = 10_000
  all_p = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
  all_q = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

  rows = len(all_p)
  cols = len(all_q)

  mean = [[0 for _ in range(cols)] for _ in range(rows)]
  var = [[0 for _ in range(cols)] for _ in range(rows)]
  
  for i in range(rows):
    for j in range(cols):
      p = all_p[i]
      q = all_q[j]
      sum_head_count = 0

      head_count = list()
      for _ in range(k):
        h_count = problem(p, q)
        head_count.append(h_count)
        sum_head_count += h_count
      
      m = sum_head_count / k

      var_sum = 0
      for x in head_count:
        var_sum += math.pow(x - m, 2)
      v = var_sum / k

      mean[i][j] = m
      var[i][j] = v
  
  print("mean")
  print_2d(mean, all_p, all_q)
  print()
  print("var")
  print_2d(var, all_p, all_q)

def print_2d(arr_2d, all_p, all_q):
  print("    q:  ", end = "")
  for q in all_q:
    print(str(q) + "      ", end = "")
  print("\n", end = "")
  print(" p   " + "-" * ((len(all_p)+1)*8))
  for i in range(len(all_p)):
    p = all_p[i]
    print(str(p) + " |  ", end = "")
    for j in range(len(all_q)):
      if (j != 0):
        print(" ", end = "")
      val = arr_2d[i][j]
      print("{value:>6.3f}".format(value=round(val, 4)), end = "")
      print("  ", end = "")
    print("\n", end="")


def problem(p, q):
  geo_count = count_geo(p)
  head_count = 0
  for _ in range(geo_count):
    if (trial(q)):
      head_count += 1
  return head_count

def count_geo(p):
  count = 0
  while True:
    count += 1
    rand = random.random()
    if (rand < p):
      break
  return count
  
def trial(q):
  rand = random.random()
  if (rand < q):
    return True
  else:
    return False
  
if (__name__ == "__main__"):
  main()

